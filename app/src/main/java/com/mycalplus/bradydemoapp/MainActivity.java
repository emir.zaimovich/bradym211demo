package com.mycalplus.bradydemoapp;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bradysdk.api.printerconnection.CutOption;
import com.bradysdk.api.printerconnection.PrinterDetails;
import com.bradysdk.api.printerconnection.PrinterProperties;
import com.bradysdk.api.printerconnection.PrinterUpdateListener;
import com.bradysdk.api.printerconnection.PrintingOptions;
import com.bradysdk.api.printerdiscovery.DiscoveredPrinterInformation;
import com.bradysdk.api.printerdiscovery.PrinterDiscovery;
import com.bradysdk.api.printerdiscovery.PrinterDiscoveryListener;
import com.bradysdk.api.templates.Template;
import com.bradysdk.printengine.printinginterface.PrinterDiscoveryFactory;
import com.bradysdk.printengine.templateinterface.TemplateFactory;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Magic starts here :D
 */
public class MainActivity extends AppCompatActivity implements PrinterDiscoveryListener, PrinterUpdateListener {
    static final String TAG = MainActivity.class.getName();
    private Button connectButton, disconnectButton, printDetailsButton, printLabelButton;
    private TextView statusTextView;

    // bluetooth adapter
    BluetoothAdapter bluetoothAdapter;

    // printed discovery object
    PrinterDiscovery printerDiscovery;
    List<PrinterDiscoveryListener> printerDiscoveryListeners;

    DiscoveredPrinterInformation discoveredPrinterInformation;
    PrinterDetails printerDetails;

    Thread connThread;

    @RequiresApi(api = Build.VERSION_CODES.S)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SimpleLogger.verbose(TAG, "onCreate()");

        setContentView(R.layout.activity_main);

        connectButton = findViewById(R.id.buttonConnect);
        connectButton.setOnClickListener(this::onConnectButton);
        printDetailsButton = findViewById(R.id.buttonPrDetails);
        printDetailsButton.setOnClickListener(this::onPrintPrDetails);
        printDetailsButton.setEnabled(Boolean.FALSE);
        printLabelButton = findViewById(R.id.buttonPrintLabel);
        printLabelButton.setOnClickListener(this::onPrintLabel);
        printLabelButton.setEnabled(Boolean.FALSE);
        disconnectButton = findViewById(R.id.buttonCancel);
        disconnectButton.setOnClickListener(this::onDisconnectButton);
        disconnectButton.setEnabled(Boolean.FALSE);
        statusTextView = findViewById(R.id.textFieldMessage);

        // check location permission
        if (!checkPermission(android.Manifest.permission.ACCESS_FINE_LOCATION))
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION});

        // check bluetooth permissions
        if (!checkPermission(android.Manifest.permission.BLUETOOTH_CONNECT) || !checkPermission(android.Manifest.permission.BLUETOOTH_SCAN))
            requestPermissions(new String[]{
                android.Manifest.permission.BLUETOOTH_CONNECT,
                Manifest.permission.BLUETOOTH_SCAN
            });

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // init printer staff
        printerDiscoveryListeners = new ArrayList<>();
        printerDiscoveryListeners.add(this); // add printer discovery implementation
        printerDiscovery = PrinterDiscoveryFactory.getPrinterDiscovery(getApplicationContext(), printerDiscoveryListeners);

        initBluetooth(); // make sure bluetooth is enabled
    }

    @Override
    protected void onResume() {
        super.onResume();
        SimpleLogger.verbose(TAG, "onResume()");

        if (printerDiscovery != null)
            printerDiscovery.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SimpleLogger.verbose(TAG, "onPause()");

        if (printerDiscovery != null)
            printerDiscovery.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SimpleLogger.verbose(TAG, "onDestroy()");

        if (printerDiscovery != null)
            printerDiscovery.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SimpleLogger.verbose(TAG, "onDestroy()");

        if (requestCode == BluetoothAdapter.STATE_ON) {
            SimpleLogger.debug(TAG, "onActivityResult() - Bluetooth enabled");
            Toast.makeText(this, "Bluetooth enabled", Toast.LENGTH_SHORT).show();
        } else
            SimpleLogger.error(TAG, "onActivityResult()", "NO Bluetooth");
    }

    /**
     * Exec. on button connect click
     *
     * @param view
     */
    private void onConnectButton(View view) {
        SimpleLogger.verbose(TAG, "onConnectButton()");

        // init & run connection thread
        if (connThread == null || !connThread.isAlive()) {
            connThread = new Thread(() -> {
                initBluetooth(); // make sure bluetooth is enabled

                if (discoveredPrinterInformation == null) { // in a case - not connected yet
                    Integer connCounter = 0, maxAttempts = 5;
                    while (discoveredPrinterInformation == null) { // printerDiscovered() sets this variable
                        if (++connCounter <= maxAttempts) {
                            printerDiscovery.startBlePrinterDiscovery(); // this works for some reason ?!

                            String connectingAttempt = "Connecting #" + (connCounter) + "/" + maxAttempts + " attempt";

                            SimpleLogger.debug(TAG, connectingAttempt);
                            runOnUiThread(() -> statusTextView.setText(connectingAttempt));
                            try {
                                Thread.sleep(1250); // make 1.5 s. pause
                            } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                            }
                        } else {
                            String noPrinterFound = "After " + maxAttempts + " attempts - NO printer found.";

                            SimpleLogger.debug(TAG, noPrinterFound);
                            runOnUiThread(() -> statusTextView.setText(noPrinterFound));

                            // reset printer info - just in case
                            discoveredPrinterInformation = null;

                            break; // exit while loop
                        }
                    }
                } else { // in a case - already connected, this must be here !!
                    printerDetails.cutLabel();

                    if (printerDetails == null) { // connecting failed, need to press power button 5 sec to regain ownership
                        String connFailed = "Connecting FAILED, press 5 sec. power button to regain ownership";

                        SimpleLogger.error(TAG, "onConnectButton()", connFailed);
                        runOnUiThread(() -> statusTextView.setText(connFailed));
                    } else {
                        String printerInfo = "Already connected to: " + discoveredPrinterInformation.getName();

                        SimpleLogger.info(TAG, printerInfo);
                        runOnUiThread(() -> statusTextView.setText(printerInfo));;
                    }
                }
            });
            connThread.start();
        } else { //already connecting
            String alreadyConnecting = "Already connecting ..";

            SimpleLogger.error(TAG, "onConnectButton()", alreadyConnecting);
            runOnUiThread(() -> Toast.makeText(getApplicationContext(), alreadyConnecting, Toast.LENGTH_SHORT).show());
        }
    }

    /**
     * Exec. on button connect click
     *
     * @param view
     */
    private void onDisconnectButton(View view) {
        SimpleLogger.verbose(TAG, "onDisconnectButton()");

        printerDiscovery.stopPrinterDiscovery();
        printerDetails.disconnect(); // disconnect printer

        discoveredPrinterInformation = null;
        printerDetails = null;

        printDetailsButton.setEnabled(Boolean.FALSE);
        disconnectButton.setEnabled(Boolean.FALSE);
        printLabelButton.setEnabled(Boolean.FALSE);
    }

    /**
     * Print printer details
     *
     * @param view
     */
    private void onPrintPrDetails(View view) {
        SimpleLogger.verbose(TAG, "onPrintPrDetails()");

        String printerDetails = getPrinterDetails(this.printerDetails);

        statusTextView.setText(printerDetails);

        SimpleLogger.debug(TAG, printerDetails);
    }

    /**
     * Print the madness
     *
     * @param view
     */
    private void onPrintLabel(View view) {
        SimpleLogger.verbose(TAG, "onPrint()");

        // figure out which template to use
        String supplyDimensions = this.printerDetails.getSupplyDimensions();
        Pattern pattern = Pattern.compile("(\\d+\\.?\\d*)");
        Matcher matcher = pattern.matcher(supplyDimensions);
        Double width = 0.0, height = 0.0;
        if (matcher.find())
            width = Double.parseDouble(matcher.group());
        if (matcher.find())
            height = Double.parseDouble(matcher.group());

        // default template
        String templateName = "hello_0_25";
        if (height == 0.5)
            templateName = "hello_0_5";
        else if (height == 0.75)
            if (width == 0.0)
                templateName = "hello_0_75";
            else if (width == 2)
                templateName = "hello_2x0_75";

        InputStream inputStream = getResources().openRawResource(getResources().getIdentifier(templateName, "raw",
            getPackageName()));

        Template template = TemplateFactory.getTemplate(inputStream, getApplicationContext());

        PrintingOptions printingOptions = new PrintingOptions();
        printingOptions.setCutOption(CutOption.EndOfJob);
        printingOptions.setNumberOfCopies(1);

        Thread printThread = new Thread(() -> {
            try {
                printerDetails.print(getApplicationContext(), template, printingOptions, Boolean.TRUE);
            } catch (Exception e) {
                SimpleLogger.error(TAG, "onPrint()", e.getMessage());
            }
        });
        printThread.start();
    }

    private boolean checkPermission(String permission) {
        SimpleLogger.debug(TAG, "checkPermission() - permission: " + permission);

        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions(String[] permissions) {
        SimpleLogger.debug(TAG, "requestPermissions() - permissions: " + Arrays.toString(permissions));

        ActivityCompat.requestPermissions(this, permissions, 0);
    }

    /**
     * Printer discovered listener
     *
     * @param discoveredPrinterInformation
     */
    @Override
    public void printerDiscovered(DiscoveredPrinterInformation discoveredPrinterInformation) {
        SimpleLogger.verbose(TAG, "printerDiscovered()");

        // init printer connection
        this.discoveredPrinterInformation = discoveredPrinterInformation;

        String printerDiscovered = "Discovered printer: " + discoveredPrinterInformation.getName();

        SimpleLogger.debug(TAG, printerDiscovered);

        Thread connectThread = new Thread(() -> {
            SimpleLogger.info(TAG, "printerDiscovered() - Connecting to: " + discoveredPrinterInformation.getName());

            this.printerDetails = printerDiscovery.connectToDiscoveredPrinter(getApplicationContext(),
                discoveredPrinterInformation,
                Collections.singletonList(MainActivity.this));

            if (this.printerDetails == null) { // connecting failed, need to press power button 5 sec to regain ownership
                String connFailed = "Connecting FAILED, press 5 sec. power button to regain ownership";

                SimpleLogger.error(TAG, "printerDiscovered()", connFailed);
                runOnUiThread(() -> statusTextView.setText(connFailed));
            } else { // connecting succeeded
                runOnUiThread(() -> {
                    statusTextView.setText(printerDiscovered);
                    printDetailsButton.setEnabled(Boolean.TRUE); // enable print details button
                    disconnectButton.setEnabled(Boolean.TRUE); // enable disconnect button
                    printLabelButton.setEnabled(Boolean.TRUE); // enable print button
                });

                // print printer details
                SimpleLogger.debug(TAG, getPrinterDetails(this.printerDetails));
            }
        });
        connectThread.start();
    }

    /**
     * Printer discovery started listener
     */
    @Override
    public void printerDiscoveryStarted() {
        SimpleLogger.debug(TAG, "printerDiscoveryStarted()");
    }

    /**
     * Printer discovery stopped listener
     */
    @Override
    public void printerDiscoveryStopped() {
        SimpleLogger.debug(TAG, "printerDiscoveryStopped()");
    }

    /**
     * Printer update listener
     * @param list
     */
    @Override
    public void PrinterUpdate(List<PrinterProperties> list) {
        SimpleLogger.info(TAG, "PrinterUpdate()");

        for (PrinterProperties printerProperties : list)
            SimpleLogger.debug(TAG, "PrinterUpdate() - printerProperties: " + printerProperties.toString());
    }

    /**
     * Init bluetooth
     */
    public void initBluetooth () {
        SimpleLogger.info(TAG, "initBluetooth()");

        if (bluetoothAdapter == null) // Bluetooth is not supported
            SimpleLogger.error(TAG, "onConnectButton()", "Bluetooth NOT SUPPORTED !!");
        else if (!bluetoothAdapter.isEnabled()) { // Bluetooth is not enabled, prompt user to turn it on
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                /*
                 * TODO: Consider calling ActivityCompat#requestPermissions
                 * here to request the missing permissions, and then overriding
                 * public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
                 * To handle the case where the user grants the permission. See the documentation
                 * for ActivityCompat#requestPermissions for more details.
                 */

                return;
            }

            startActivityForResult(enableBtIntent, BluetoothAdapter.STATE_ON);
        }
    }

    /**
     * Get printer details
     *
     * @param printerDetails
     */
    public String getPrinterDetails (PrinterDetails printerDetails) {
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy MMMM dd HH:mm:ss");

        if (printerDetails != null)
            return " Printer name: " + printerDetails.getPrinterName() +
                ",\n Printer model: " + printerDetails.getPrinterModel() +
                ",\n Printer status: " + printerDetails.getPrinterStatus() +
                ",\n Printer have ownership: " + printerDetails.haveOwnership() +
                ",\n Printer status message: " + printerDetails.getPrinterStatusMessage() +
                ",\n Printer status message tittle: " + printerDetails.getPrinterStatusMessageTitle() +
                ",\n Printer status remedy expl.: " + printerDetails.getPrinterStatusRemedyExplanationMessage() +
                ",\n Printer supply name: " + printerDetails.getSupplyName() +
                ",\n Printer supply remaining: " + printerDetails.getSupplyRemainingPercentage() + " %" +
                ",\n Printer supply dimensions: " + printerDetails.getSupplyDimensions() + " inch" +
                ",\n Printer connection type: " + printerDetails.getConnectionType() +
                ",\n Printer battery lvl. : " + printerDetails.getBatteryLevelPercentage() + " %" +
                ",\n Is AC connected: " + printerDetails.getBatteryLevelPercentage() +
                ",\n Printer ribbon: " + printerDetails.getRibbonName() +
                ",\n Printer ribbon remaining: " + printerDetails.getRibbonRemainingPercentage() + "%" +
                ",\n Substrate width in: " + printerDetails.getSubstrateWidthInInches() + " inch" +
                ",\n Substrate height in: " + printerDetails.getSubstrateHeightInInches() + " inch" +
                ",\n Data fetched: " + localDateTime.format(formatter);
        else
            return "Printer unavailable";
    }
}