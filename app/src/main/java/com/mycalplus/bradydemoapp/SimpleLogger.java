package com.mycalplus.bradydemoapp;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Simple logger class that logs msg as Verbose/Info/Error, and save it in file on SD card. It can be configured
 * from {@link AppConfig} to custom log any package.
 * <p>
 * Note:
 * Production environment - logging is only in *.log file.
 * Development environment - logging is both in Logcat and *.log file.
 */
public class SimpleLogger {
    private static final String VERBOSE = "Verbose: ", INFO = "Info: ", ERROR = "Error: ";

    /**
     * Log msg as Verbose
     *
     * @param tag
     * @param msg
     */
    public static void verbose(String tag, String msg) {
        boolean logPackage = logPackage(tag);

        if (AppConfig.enableLogging) {
            if (AppConfig.logVerbose && logPackage)
                Log.v(tag, msg);

            if (AppConfig.logSystemOut && logPackage)
                sysOut(tag, VERBOSE + msg);
        }

        if (AppConfig.writeLogToAFile && logPackage)
            appendLogFile(tag, VERBOSE + msg);
    }

    /**
     * Log msg as Info
     *
     * @param tag
     * @param msg
     */
    public static void info(String tag, String msg) {
        boolean logPackage = logPackage(tag);

        if (AppConfig.enableLogging) {
            if (AppConfig.logInfo && logPackage)
                Log.i(tag, msg);

            if (AppConfig.logSystemOut && logPackage)
                sysOut(tag, INFO + msg);
        }

        if (AppConfig.writeLogToAFile && logPackage)
            appendLogFile(tag, INFO + msg);
    }

    /**
     * Log msg as Info
     *
     * @param tag
     * @param msg
     */
    public static void debug(String tag, String msg) {
        boolean logPackage = logPackage(tag);

        if (AppConfig.enableLogging) {
            if (AppConfig.logInfo && logPackage)
                Log.d(tag, msg);

            if (AppConfig.logSystemOut && logPackage)
                sysOut(tag, INFO + msg);
        }

        if (AppConfig.writeLogToAFile && logPackage)
            appendLogFile(tag, INFO + msg);
    }

    /**
     * Log msg as Error
     * <p>
     * NOTE: log throwable
     *
     * @param tag
     * @param method
     * @param throwable
     */
    public static void error(String tag, String method, Throwable throwable) {
        boolean logPackage = logPackage(tag);

        if (AppConfig.enableLogging) {
            if (AppConfig.logError && logPackage)
                Log.e(tag, method, throwable);

            if (AppConfig.logSystemOut && logPackage)
                sysOut(tag, ERROR + method + " " + throwable.getLocalizedMessage());
        }

        if (AppConfig.writeLogToAFile && logPackage)
            appendLogFile(tag, ERROR + method + " " + throwable.getLocalizedMessage());
    }

    /**
     * Log msg as Error
     * <p>
     * NOTE: log msg
     *
     * @param tag
     * @param method
     * @param msg
     */
    public static void error(String tag, String method, String msg) {
        boolean logPackage = logPackage(tag);

        if (AppConfig.enableLogging) {
            if (AppConfig.logError && logPackage)
                Log.e(tag, method + ": " + msg);

            if (AppConfig.logSystemOut && logPackage)
                sysOut(tag, ERROR + method + " " + msg);
        }

        if (AppConfig.writeLogToAFile && logPackage)
            appendLogFile(tag, ERROR + method + " " + msg);
    }

    /**
     * Build system out from log
     * <p>
     * NOTE:
     * In some cases this is needed, rarely but it is needed
     *
     * @param tag
     * @param msg
     */
    private static void sysOut(String tag, String msg) {
        SimpleDateFormat sdf = new SimpleDateFormat(AppConfig.dateTimeFormat);

        System.out.println(sdf.format(new Date()) + ", " + tag + ", " + msg);
    }

    /**
     * Append string to log file
     * <p>
     * NOTE: Log file name is based on date and it has extension *.log
     *
     * @param tag
     * @param msg
     */
    private static void appendLogFile(String tag, String msg) {
        // basically - do nothing
    }

    /**
     * Decide weather or not to log package represented by tag
     *
     * @param tag
     * @return
     */
    private static boolean logPackage(String tag) {
        // by default log all
        return Boolean.TRUE;
    }

    /**
     * Decide weather or not to log sub package
     * @param tag
     * @return
     */
    private static boolean logSubPackage(String tag) {
        Boolean logSubPackage = Boolean.TRUE;

        if (tag.contains("db")) // log db sub package
            logSubPackage = AppConfig.logDbSubPackage;

        else if (tag.contains("gui")) // log gui sub package
            logSubPackage = AppConfig.logGuiSubPackage;

        else if (tag.contains("prefs")) // log prefs sub package
            logSubPackage = AppConfig.logPrefsSubPackage;

        else if (tag.contains("rest")) // log rest sub package
            logSubPackage = AppConfig.logRestSubPackage;

        else if (tag.contains("util")) // log util sub package
            logSubPackage = AppConfig.logUtilSubPackage;

        else if (tag.contains("sync")) // log sync sub package
            logSubPackage = AppConfig.logSyncSubPackage;

        return logSubPackage;
    }
}