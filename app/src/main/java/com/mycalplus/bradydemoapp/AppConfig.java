package com.mycalplus.bradydemoapp;

/**
 * General application properties/settings all in one place, environments, server in use, default names and values, url's...
 */
public class AppConfig {
    /////////////////////////////////////////////////////////////
    // Logging
    /////////////////////////////////////////////////////////////

    /**
     * Enable/Disable app logging
     *
     * Enable - logging IS displayed in logcat
     * Disable - logging is NOT displayed in logcat
     *
     * DEFAULT: disable it on production server
     * NOTE: What ever option is chosen, logging is written in SD/MyCal+/logs folder
     */
    public static Boolean enableLogging = Boolean.TRUE;

    /**
     * Write log data in file
     */
    public static Boolean writeLogToAFile = Boolean.TRUE;

    /**
     * Verbose logging level
     */
    public static Boolean logVerbose = Boolean.TRUE;

    /**
     * Info logging level
     */
    public static Boolean logInfo = Boolean.TRUE;

    /**
     * Error logging level
     */
    public static Boolean logError = Boolean.TRUE;

    // -------------------------

    /**
     * Enable System.out.println with logging, used mostly for testing, because
     * normal logging does not work :/
     */
    public static Boolean logSystemOut = Boolean.FALSE;

    /**
     * Log constructor(s), log or not log object constructors,
     * this is to avoid unnecessary logging
     */
    public static Boolean logConstructor = Boolean.FALSE;

    /**
     * Display & write logs form adapters
     */
    public static Boolean logAdapters = Boolean.FALSE;

    // -------------------------

    /**
     * Display & write logs from activity package
     */
    public static Boolean logActivityPackage = Boolean.TRUE;

    /**
     * Display & write logs from background package
     */
    public static Boolean logBackgroundPackage = Boolean.TRUE;

    /**
     * Display & write logs from common package
     */
    public static Boolean logCommonPackage = Boolean.TRUE;

    /**
     * Display & write logs from modules package
     */
    public static Boolean logModulesPackage = Boolean.TRUE;

    /**
     * Display & write logs form Widget package
     */
    public static Boolean logWidgetPackage = Boolean.FALSE;

    /**
     * Display & write logs form MyApplication class
     */
    public static Boolean logMyApplication = Boolean.FALSE;

    // -------------------------

    /**
     * Display & write logs from db sub package
     * <p>
     * sub package START
     */
    public static Boolean logDbSubPackage = Boolean.FALSE;

    /**
     * Display & write logs from gui sub package
     */
    public static Boolean logGuiSubPackage = Boolean.TRUE;

    /**
     * Display & write logs from common/prefs sub package
     */
    public static Boolean logPrefsSubPackage = Boolean.FALSE;

    /**
     * Display & write logs from rest sub package
     *
     * In PRODUCTION environment do not log REST data - security leak -> token
     */
    public static Boolean logRestSubPackage = enableLogging && Boolean.TRUE;

    /**
     * Display & write logs from common/util sub package
     */
    public static Boolean logUtilSubPackage = Boolean.FALSE;

    /**
     * Display & write logs from sync syb package
     *
     * sub package END
     */
    public static Boolean logSyncSubPackage = Boolean.TRUE;

    /////////////////////////////////////////////////////////////
    // Date & Time
    /////////////////////////////////////////////////////////////

    /**
     * Default date time format across the app
     */
    public static String dateTimeFormat = "yyyy-MM-dd HH:mm:ss";

    /////////////////////////////////////////////////////////////
    // End
    /////////////////////////////////////////////////////////////
}

